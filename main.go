package main

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	"os"
	"text/template"
)

type resource struct {
	Mode      string     `yaml:"mode"`
	Type      string     `yaml:"type"`
	Name      string     `yaml:"name"`
	Provider  string     `yaml:"provider"`
	Instances []instance `yaml:"instances"`
}
type instance struct {
	Attributes   map[string]interface{} `yaml:"attributes"`
	Dependencies []string               `yaml:"dependencies"`
}

type stateFile struct {
	Outputs   []map[string]interface{} `yaml:"outputs"`
	Resources []resource               `yaml:"resources"`
}

func (s *stateFile) Parse(data []byte) error {
	return yaml.Unmarshal(data, s)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Usage:
// terraformStateHelper <input state file> <input template file> <output inventory file>
// TODO: Support for multiple providers
// TODO: Support for multiple state files
// TODO: GUI?

func main() {
	var hosts []instance
	statefile := os.Args[1]
	templateInput := os.Args[2]
	templateOutput := os.Args[3]
	user := os.Args[4]
	keyfile := os.Args[5]
	statedata, err := ioutil.ReadFile(statefile)

	var state stateFile
	err = state.Parse(statedata)
	for _, r := range state.Resources {
		if r.Mode == "managed" && (r.Type == "aws_instance" || r.Type == "vsphere_virtual_machine") {
			for _, i := range r.Instances {
				hosts = append(hosts, i)
				//fmt.Printf("%s\n", i.Attributes["tags"].(map[string]interface{})["Name"])
			}
		}
	}

	data := make(map[string]interface{})
	data["Hosts"] = hosts
	data["User"] = user
	data["Keyfile"] = keyfile

	tpl, err := template.ParseFiles(templateInput)
	checkErr(err)
	file, err := os.Create(templateOutput)
	err = tpl.Execute(file, data)

}
